var JPromise = require("../lib/jp_promise");

exports.resolved = JPromise.resolved;

exports.rejected = function(reason) {
    var deferred = new JPromise.deferred();
    deferred.reject(reason);
    return deferred.promise;
};

exports.deferred = function () {
    var deferred = new JPromise.deferred();
    deferred.fulfill = deferred.resolve;
    return deferred;
};