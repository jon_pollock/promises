"use strict";

module.exports = function (grunt) {
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-uglify");

    grunt.initConfig({
        jshint: {
            options: { jshintrc: __dirname + '/.jshintrc' },
            files: ['lib/*.js']
        },

        uglify: {
            "jp_promise.min.js": ["lib/jp_promise.js"],
            options: {
                report: "gzip"
            }
        }

    });

    grunt.registerTask("default", ["jshint", "uglify"]);
};
