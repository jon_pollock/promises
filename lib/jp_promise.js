/* global MessageChannel */
(function(exports) {
    "use strict";

    var UNDEFINED = 'undefined';

    /**
     * @const
     * @type {string}
     */
    var PENDING = "pending";

    /**
     * @const
     * @type {string}
     */
    var FULFILLED = "fullfilled";

    /**
     * @const
     * @type {string}
     */
    var REJECTED = "rejected";

    /**
     * @type {number}
     */
    var promise_count = 0;

    /**
     * A NO Operation function. It does nothing and returns nothing.
     * @const
     * @type {function()}
     */
    var NOOP = function() { };

    /**
     * This function will at the next available time slice. The function
     * begin called takes no arguments and it's context will be set to the
     * global object, usually window.
     *
     * This function may optimize the way it dispatches the method depending on
     * the environment in which this code is running. By default, it uses
     * {@link window."#setTimeout"} with a timeout of 0.
     *
     * Depending on the environment, different implementations will be used. In
     * the case of a NodeJS environment, it will use process.nextTick. If running
     * in a ModernBrowser, a MessageChannel will be used. In all other cases,
     * a simple setTimeout(fn, 0) will be used.
     *
     * @type {function(function)}
     */
    var nextTick = NOOP;

    if ((typeof process !== UNDEFINED) && process.nextTick) {
        // Node nextTick
        nextTick = process.nextTick;

    } else if (typeof MessageChannel !== UNDEFINED) {
        // Modern Browsers
        var nextTickChannel = new MessageChannel();
        var nextTickQueue = [];
        nextTickChannel.port1.onmessage = function() {
            if (nextTickQueue.length) {
                (nextTickQueue.shift())();
            }
        };
        nextTick = function(fn) {
            nextTickQueue.push(fn);
            nextTickChannel.port2.postMessage(0);
        };

    } else {
        // Fallback to using setTimeout
        nextTick = function nextTick(fn) {
            setTimeout(fn, 0);
        };
    }


    /**
     * A Promise instance.
     * @constructor
     * @param {function} onFulfilled - function to be called on resolution
     * @param {function} onRejected  - function to be called on rejection
     */
    function Promise(onFulfilled, onRejected) {
        this.state = PENDING;
        this.id = ++promise_count;
        this.promises = false;
        
        if (typeof onFulfilled === "function") {
           this.onFulfilled = onFulfilled;
        }
        if (typeof onRejected === "function") {
            this.onRejected = onRejected;
        }
    }



    /**
     * This internal method is used to implement the resolve logic depending on the
     * state of the promise and the type of value.
     *
     * @param {Promise} promise - the promise object that is being resolved.
     * @param {object} x - the value with which the promise is being resolved.
     */
    function resolvePromise(promise, x) {
        if (promise === x) {
            throw new TypeError("The parameters, 'promise' and 'x', are the same object.");
        }

        var xType = typeof x;
        if ((x !== null) && ((xType === "function") || (xType === "object"))) {
            try {
                var then = x.then;
                if (typeof then === "function") {
                    var callLock = false;
                    try {
                        then.call(x,
                            function(y) {
                                if (callLock) { return; } callLock = true;
                                resolvePromise(promise, y);
                            },
                            function(r) {
                                if (callLock) { return; } callLock = true;
                                promise.reject(r);
                            }
                        );
                    } catch (e) {
                        if (callLock) { return; } callLock = true;
                        promise.reject(e);
                    }
                } else {
                    promise.resolve(x);
                }
            } catch (e) {
                promise.reject(e);
            }
            return;
        } else {
            promise.resolve(x);
        }
    }



    function runOnFulfilled(promise, callback) {
        var value = promise.value;
        var onFulfilled = promise.onFulfilled;
        if (onFulfilled) {
            nextTick(function() {
                try {
                    var retValue = onFulfilled(value);
                    if (retValue === promise) {
                        throw new TypeError("Return value from onFulfilled cannot be the promise itself.");
                    }
                    value = retValue;
                    resolveAllPromises(promise, value);
                    callback();
                } catch (e) {
                    promise.state = REJECTED;
                    promise.reason = e;
                    rejectAllPromises(promise, e);
                }
            });
        } else {
            resolveAllPromises(promise, value);
            callback();
        }
    }

    function runOnRejected(promise, callback) {
        var reason = promise.reason;
        var onRejected = promise.onRejected;
        if (onRejected) {
            nextTick(function() {
                try {
                    var retValue = onRejected(reason);
                    if (retValue === promise) {
                        throw new TypeError("Return value from onRejected cannot be the promise itself.");
                    }
                    reason = retValue;
                    resolveAllPromises(promise, reason);
                } catch (e) {
                    rejectAllPromises(promise, e);
                }
                callback();
            });
        } else {
            rejectAllPromises(promise, reason);
            callback();
        }
    }


    function resolveAllPromises(promise, value) {
        while (promise.promises && promise.promises.length > 0) {
            resolvePromise(promise.promises.shift(), value);
        }
    }


    function rejectAllPromises(promise, reason) {
        while (promise.promises && promise.promises.length > 0) {
            promise.promises.shift().reject(reason);
        }
    }

    /**
     * Internal utility function to add a promise to the current promise. If the current
     * list of promises is false, which is the initial state, it will set it to a newly
     * constructed array with the passed in promise as it's only item.
     *
     * Otherwise, it will append the promise to the array.
     *
     * @this {Promise}
     * @param {Promise} promise - the dependent promise to add to this promise.
     */
    function addPromise(promise) {
        /* jshint validthis:true */
        if (this.promises === false) {
            this.promises = [promise];
        } else {
            this.promises.push(promise);
        }
    }


    Promise.prototype = {
        onFulfilled: false,
        onRejected: false,

        value: false,
        reason: false,
        processed: false,

        isPending: function isPending() { return this.state === PENDING; },
        isFulfilled: function isFulfilled() { return this.state === FULFILLED; },
        isRejected: function isRejected() { return this.state === REJECTED; },

        then: function(onResolve, onRejected) {
            var self = this;
            var newPromise = new Promise(onResolve, onRejected);

            if (self.isPending()) {
                addPromise.call(this, newPromise);
            } else if (self.isFulfilled()) {
                nextTick(function() {
                    newPromise.value = self.value;
                    runOnFulfilled(newPromise, NOOP);
                });
            } else if (self.isRejected()) {
                nextTick(function() {
                    newPromise.reason = self.reason;
                    runOnRejected(newPromise, NOOP);
                });
            }

            return newPromise;
        },


        resolve: function(value) {
            var self = this;
            if (self.isPending() && !self.processed) {
                self.processed = true;
                self.value = value;
                runOnFulfilled(self, function() {
                    self.state = FULFILLED;
                });
            }
        },



        reject: function(reason) {
            var self = this;
            if (self.isPending() && !self.processed) {
                self.processed = true;
                self.reason = reason;
                runOnRejected(self, function() {
                    self.state = REJECTED;
                });
            }
        }
    };



    function Deferred() {
        var p = new Promise();
        this.promise = p;
        this.resolve = function(value) { p.resolve(value); };
        this.reject = function(reason) { p.reject(reason); };
    }


    exports.promise = Promise;
    exports.deferred = Deferred;

    exports.resolved = function resolved(value) {
        var promise = new Promise();
        promise.resolve(value);
        return promise;
    };

    exports.rejected = function rejected(reason) {
        var promise = new Promise();
        promise.reject(reason);
        return promise;
    };

})(typeof exports === 'undefined' ? this['JPromise']={} : exports);