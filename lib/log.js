/** @module log */
(function(module) {

    /**
     * This method will take any object and convert it to a string for logging purposes.
     *
     * @param {*} obj - the object to convert to a string.
     * @return {string}
     */
    function objectToString(obj) {
        var str = '';
        if (obj === null) {
            str = 'null';
        } else if (obj === void 0) {
            str = '_undefined_';
        } else if (typeof obj === 'object') {
            for (var p in obj) {
                str += '{object';
                if (obj.hasOwnProperty(p)) {
                    str += ' ' + p + ': ' + objectToString(obj[p]);
                }
                str += '}';
            }
        } else if (typeof obj === 'function') {
            str = 'function';
        } else if (typeof obj === 'string') {
            str = "'" + obj + "'";
        } else {
            str = obj.toString();
        }
        return str;
    }

    /*
     * This is a helper method that will log the given message.
     */
    module.exports = function() {
        var len = arguments.length, msg;
        if (len === 0) {
            return;
        } else if (arguments.length === 1 && typeof arguments[0] === 'string') {
            console.log(arguments[0]);
        } else {
            msg = [];
            for (var i=0; i<len; i++) {
                if (typeof arguments[i] === 'string') {
                    msg.push(arguments[i]);
                } else {
                    msg.push(objectToString(arguments[i]));
                }
            }
            console.log(msg.join(''));
        }
    };

})(typeof module === 'undefined' ? this : module);
